import styled from 'styled-components/native'

import { getStatusBarHeight } from 'react-native-iphone-x-helper'

import { colors, fonts } from '~/styles/global'

const paddingTop = `${getStatusBarHeight(true) + 12}`
const padding = `${paddingTop}px 24px 50px`

export const Container = styled.View`
  padding: ${padding};
  background-color: ${colors.purple400};
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`

export const Tasks = styled.View`
  align-items: center;
  flex-direction: row;
`
export const TaskCounter = styled.Text`
  font-size: 15px;
  color: ${colors.white};
  font-family: ${({ bold }) => (bold ? fonts.bold : fonts.medium)};
`
