import React from 'react'
import { Image } from 'react-native'

import logoImg from '~/assets/images/logo/logo.png'

import { Container, TaskCounter, Tasks } from './styles'

export function Header({ tasksCounter }) {
  const tasksCounterText = tasksCounter === 1 ? 'tarefa' : 'tarefas'

  return (
    <Container>
      <Image source={logoImg} />
      <Tasks>
        <TaskCounter>Você tem </TaskCounter>
        <TaskCounter bold>
          {tasksCounter} {tasksCounterText}
        </TaskCounter>
      </Tasks>
    </Container>
  )
}
