import styled from 'styled-components/native'

import { Form } from '@unform/mobile'

import { colors } from '~/styles/global'

export const FormWrapper = styled.View`
  background-color: ${colors.white};
  flex-direction: row;
  align-items: center;
  border-radius: 5px;
  margin: -36px 18px auto;
  padding-top: 6px;
  padding-bottom: 6px;
`

export const TaskForm = styled(Form)`
  margin-right: 12px;
  margin-left: 12px;
`

export const InputDivisor = styled.View`
  height: 20px;
`

export const ButtonWrapper = styled.View`
  align-items: center;
  justify-content: center;
  ${({ row }) => row && 'flex-direction: row;'}
`

export const Button = styled.TouchableOpacity`
  height: ${({ height }) => (height ? `${height}px` : '40px')};
  width: ${({ width }) => (width ? `${width}px` : '102px')};
  background-color: ${colors.white};
  border-radius: 5px;
  margin-top: 6px;
  margin-left: 4px;
  margin-right: 4px;
  justify-content: center;
  align-items: center;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
`

export const ButtonText = styled.Text`
  font-size: ${({ fontSize }) => (fontSize ? `${fontSize}px` : '12px')};
  font-weight: bold;
`
