import React, { useEffect, useRef, useState } from 'react'

import { connect } from 'react-redux'

import { colors } from '~/styles/global'

import { Input } from '../Input'

import {
  FormWrapper,
  TaskForm,
  InputDivisor,
  ButtonWrapper,
  Button,
  ButtonText
} from './styles'

const mapStateToProps = state => {
  return {
    tasks: state.tasks
  }
}

export const TodoInput = connect(mapStateToProps)(
  ({
    tasks,
    addTask,
    filterDoneTasks,
    filterNotDoneTasks,
    orderTasksByDate
  }) => {
    const formRef = useRef(null)

    const [taskTitle, setTaskTitle] = useState('')
    const [taskDescription, setTaskDescription] = useState('')
    const [taskDate, setTaskDate] = useState('')
    const [taskHour, setTaskHour] = useState('')

    const handleNewTaskAddition = newTask => addTask(newTask)

    const clearFormFields = () => {
      setTaskTitle('')
      setTaskDescription('')
      setTaskDate('')
      setTaskHour('')
    }

    const handleFormSubmit = () => formRef.current.submitForm()

    useEffect(() => clearFormFields(), [tasks])

    return (
      <>
        <FormWrapper>
          <TaskForm ref={formRef} onSubmit={handleNewTaskAddition}>
            <Input
              color={colors.black}
              value={taskTitle}
              onChangeText={setTaskTitle}
              name="title"
              placeholder="título"
            />
            <InputDivisor />
            <Input
              color={colors.black}
              value={taskDescription}
              onChangeText={setTaskDescription}
              name="description"
              placeholder="descrição"
              maxLength={100}
            />
            <InputDivisor />
            <Input
              value={taskDate}
              onChangeText={setTaskDate}
              color={colors.black}
              name="date"
              placeholder="data"
              maxLength={10}
            />
            <InputDivisor />
            <Input
              value={taskHour}
              onChangeText={setTaskHour}
              color={colors.black}
              name="hour"
              placeholder="horário"
              maxLength={5}
            />
          </TaskForm>
        </FormWrapper>
        <ButtonWrapper>
          <Button
            activeOpacity={0.5}
            onPress={handleFormSubmit}
            height={42}
            width={325}
          >
            <ButtonText fontSize={18}>Adicionar tarefa</ButtonText>
          </Button>
        </ButtonWrapper>
        <ButtonWrapper row>
          <Button activeOpacity={0.5} onPress={() => filterDoneTasks()}>
            <ButtonText>Tarefas</ButtonText>
            <ButtonText>Finalizadas</ButtonText>
          </Button>
          <Button activeOpacity={0.5} onPress={() => filterNotDoneTasks()}>
            <ButtonText>Tarefas não</ButtonText>
            <ButtonText>Finalizadas</ButtonText>
          </Button>
          <Button activeOpacity={0.5} onPress={() => orderTasksByDate()}>
            <ButtonText>Ordenar tarefas</ButtonText>
            <ButtonText>por data</ButtonText>
          </Button>
        </ButtonWrapper>
      </>
    )
  }
)
