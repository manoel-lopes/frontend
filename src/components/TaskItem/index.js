import React, { useState, useRef, useEffect } from 'react'
import { Image, TouchableOpacity } from 'react-native'

import Icon from 'react-native-vector-icons/Feather'

import trashIcon from '~/assets/icons/trash/trash.png'
import editIcon from '~/assets/icons/edit/edit.png'

import { colors } from '~/styles/global'

import {
  Container,
  InfoWrapper,
  TaskButton,
  TaskText,
  TaskMarker,
  TaskDateText,
  IconsWrapper,
  IconsDivider
} from './styles'

export const TaskItem = ({
  task,
  toggleTaskDone,
  removeTask,
  editTask,
  shouldDisableTaskButton = false,
  setIsSomeTaskBeingEdited
}) => {
  const [isBeingEdited, setIsBeingEdited] = useState(false)
  const [taskNewTitle, setTaskNewTitle] = useState(task.title)
  const textInputRef = useRef(null)

  const handleTaskEditing = isTaskBeingEdited => {
    setIsBeingEdited(isTaskBeingEdited)
    setIsSomeTaskBeingEdited(isTaskBeingEdited)
  }

  const handleStartEditing = () => handleTaskEditing(true)

  const handleCancelEditing = () => {
    setTaskNewTitle(task.title)
    handleTaskEditing(false)
  }

  const handleSubmitEditing = () => {
    editTask(task.id, taskNewTitle)
    handleTaskEditing(false)
  }

  useEffect(() => {
    if (textInputRef.current && isBeingEdited) {
      textInputRef.current.focus()
    }
  }, [isBeingEdited])

  return (
    <Container>
      <InfoWrapper>
        <TaskButton
          disabled={shouldDisableTaskButton}
          activeOpacity={0.7}
          disabled={isBeingEdited}
          onPress={() => !isBeingEdited && toggleTaskDone(task.id)}
        >
          <TaskMarker done={task.done}>
            {task.done && (
              <Icon name="check" size={12} color={colors.white} />
            )}
          </TaskMarker>

          <TaskText
            underlineColorAndroid="transparent"
            done={task.done}
            value={taskNewTitle}
            editable={isBeingEdited}
            onChangeText={setTaskNewTitle}
            onSubmitEditing={handleSubmitEditing}
            ref={textInputRef}
          />
          <TaskDateText>{`${task.date} ${task.hour}`}</TaskDateText>
        </TaskButton>
      </InfoWrapper>

      <IconsWrapper>
        {isBeingEdited ? (
          <TouchableOpacity onPress={handleCancelEditing}>
            <Icon name="x" size={26} color={colors.gray200} />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={handleStartEditing}>
            <Image source={editIcon} />
          </TouchableOpacity>
        )}

        <IconsDivider />

        <TouchableOpacity
          disabled={isBeingEdited}
          onPress={() => removeTask(task.id)}
        >
          <Image
            source={trashIcon}
            style={{ opacity: isBeingEdited ? 0.2 : 1 }}
          />
        </TouchableOpacity>
      </IconsWrapper>
    </Container>
  )
}
