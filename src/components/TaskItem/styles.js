import styled from 'styled-components/native'

import { Platform } from 'react-native'

import { colors, fonts } from '~/styles/global'

export const Container = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

export const InfoWrapper = styled.View`
  flex: 1;
`

export const TaskButton = styled.TouchableOpacity`
  flex: 1;
  padding: 16px 24px;
  border-radius: 4px;
  flex-direction: row;
  align-items: center;
`

export const TaskText = styled.TextInput`
  font-size: 14px;
  height: 25px;
  color: ${({ done }) => (done ? colors.green400 : colors.gray200)};
  font-family: ${({ done }) => (done ? fonts.bold : fonts.regular)};
  padding-bottom: 8px;
  ${({ done }) =>
    done &&
    `text-decoration: line-through;
    text-decoration-color: ${colors.green400};`}
`

export const TaskDateText = styled.Text`
  font-size: 12px;
  color: ${colors.gray200};
  margin-left: 58px;
  position: absolute;
  padding-top: 30px;
`

export const TaskMarker = styled.View`
  height: 18px;
  width: 18px;
  align-items: center;
  justify-content: center;
  border-radius: 4px;
  margin-right: 15px;
  ${({ done }) => done && `background-color: ${colors.green400};`}
  ${({ done }) =>
    !done &&
    `border-width: 1px;
    border-color: ${colors.gray200};`}
`

export const IconsWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  padding-left: 12px;
  padding-right: 24px;
`
export const IconsDivider = styled.View`
  height: 24px;
  width: 1px;
  margin: auto 12px;
  background-color: rgba(196, 196, 196, 0.24);
`
