import styled from 'styled-components/native'

export const Container = styled.TextInput`
  font-size: 16px;
  font-weight: bold;
  color: ${props => (props.color ? props.color : '#fff')};
`
