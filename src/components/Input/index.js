import React, { useRef, useEffect, useCallback } from 'react'

import { useField } from '@unform/core'

import { colors } from '~/styles/global'

import { Container } from './styles'

export const Input = ({ name, onChangeText, ...rest }) => {
  const inputRef = useRef(null)

  const { fieldName, registerField } = useField(name)

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      getValue() {
        if (inputRef.current) {
          return inputRef.current.value
        }
        return ''
      }
    })
  }, [fieldName, registerField])

  const handleChangeText = useCallback(
    text => {
      if (inputRef.current) inputRef.current.value = text
      if (onChangeText) onChangeText(text)
    },
    [onChangeText]
  )

  return (
    <>
      <Container
        ref={inputRef}
        onChangeText={handleChangeText}
        placeholderTextColor={colors.gray200}
        selectionColor={colors.gray500}
        {...rest}
      />
    </>
  )
}
