import React, { useState } from 'react'
import { Platform } from 'react-native'
import { colors } from '~/styles/global'

import { ItemWrapper } from '../ItemWrapper'
import { TaskItem } from '../TaskItem'

import { Container } from './styles'

export const TasksList = ({
  tasks,
  toggleTaskDone,
  removeTask,
  editTask,
  shouldDisableTaskButton
}) => {
  const [isSomeTaskBeingEdited, setIsSomeTaskBeingEdited] = useState(false)

  const isTaskingBeingEditedInIOS =
    isSomeTaskBeingEdited && Platform.OS === 'ios'

  return (
    <Container
      data={tasks}
      keyExtractor={item => String(item.id)}
      removeClippedSubviews={false}
      contentContainerStyle={{
        paddingBottom: isTaskingBeingEditedInIOS ? 260 : 0
      }}
      renderItem={({ item, index }) => {
        return (
          <ItemWrapper index={index}>
            <TaskItem
              task={item}
              toggleTaskDone={toggleTaskDone}
              removeTask={removeTask}
              editTask={editTask}
              shouldDisableTaskButton={shouldDisableTaskButton}
              setIsSomeTaskBeingEdited={setIsSomeTaskBeingEdited}
            />
          </ItemWrapper>
        )
      }}
    />
  )
}
