import React from 'react'

import { createStackNavigator } from '@react-navigation/stack'

import { Start } from '~/pages/Start'
import { Login } from '~/pages/Login'
import { SignIn } from '~/pages/SignIn'
import { Home } from '~/pages/Home'

const StackNavigation = createStackNavigator()

export const StackRoutes = () => (
  <StackNavigation.Navigator
    screenOptions={{
      headerShown: false,
      gestureEnabled: false
    }}
  >
    <StackNavigation.Screen name="Start" component={Start} />

    <StackNavigation.Screen name="Login" component={Login} />

    <StackNavigation.Screen name="SignIn" component={SignIn} />

    <StackNavigation.Screen name="Home" component={Home} />
  </StackNavigation.Navigator>
)
