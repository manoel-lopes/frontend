import AsyncStorage from '@react-native-async-storage/async-storage'

const getUsers = async () => {
  const response = await AsyncStorage.getItem('@users')

  if (response) {
    const users = JSON.parse(response)

    return users
  }

  return []
}

const storeUser = async user => {
  const response = await AsyncStorage.getItem('@users')
  let users = []

  if (response) {
    users = JSON.parse(response)
    users.push(user)
  }

  AsyncStorage.setItem('@users', JSON.stringify(users))
}

const updateUser = async user => {
  const response = await AsyncStorage.getItem('@users')
  let users = []

  if (response) {
    const { id } = user
    users = JSON.parse(response)
    const indexOfUserToBeUpdated = users.findIndex(user => user.id === id)

    if (indexOfUserToBeUpdated !== -1) {
      users[indexOfUserToBeUpdated] = user
      AsyncStorage.setItem('@users', JSON.stringify(users))
    }
  }
}

export const api = {
  getUsers,
  storeUser,
  updateUser
}
