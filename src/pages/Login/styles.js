import styled from 'styled-components/native'

import { colors } from '~/styles/global'

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: ${colors.purple400};
`

export const FormWrapper = styled.View`
  padding-bottom: 25px;
`

export const InputDivisor = styled.View`
  height: 1px;
  width: 300px;
  margin-top: 4px;
  margin-bottom: 25px;
  background-color: ${colors.white};
`

export const ButtonWrapper = styled.View`
  top: -30px;
  align-items: center;
  justify-content: center;
`

export const Button = styled.TouchableOpacity`
  height: 52px;
  width: 165px;
  background-color: ${({ backgroundColor }) =>
    backgroundColor ? backgroundColor : colors.white};
  justify-content: center;
  align-items: center;
  border-radius: 25px;
  border-top-right-radius: 25px;
  border-bottom-right-radius: 25px;
`

export const ButtonText = styled.Text`
  font-size: ${({ fontSize }) => (fontSize ? fontSize : 22)}px;
  font-weight: bold;
  color: ${({ color }) => (color ? color : colors.black)};
`
