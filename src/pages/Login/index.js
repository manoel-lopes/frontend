import React, { useRef } from 'react'
import { Alert } from 'react-native'

import { connect } from 'react-redux'

import { useNavigation } from '@react-navigation/core'

import { Form } from '@unform/mobile'

import { api } from '~/services/api'

import { setNewUser } from '~/store/actions/user'

import { colors } from '~/styles/global'

import { Input } from '~/components/Input'

import {
  Container,
  FormWrapper,
  InputDivisor,
  ButtonWrapper,
  Button,
  ButtonText
} from './styles'

const mapDispatchToProps = dispatch => {
  return {
    setUser: newUser => {
      const action = setNewUser(newUser)
      dispatch(action)
    }
  }
}

export const Login = connect(
  null,
  mapDispatchToProps
)(({ setUser }) => {
  const formRef = useRef(null)
  const { navigate } = useNavigation()

  const handleStoreNewUser = user => {
    const { email, password } = user

    if (!email || !password) {
      return Alert.alert(
        'Campos obrigatórios',
        'Você deve preecncher todos os campos para cadastrar a tarefa'
      )
    }

    const emailTest = /[\w.]+@[\w]+\.[a-z]+(\.[a-z]+)?$/i
    const isValidEmail = emailTest.test(email)

    if (email && !isValidEmail) {
      return Alert.alert(
        'Email inválido',
        'Você deve digitar um email válido'
      )
    }

    api.getUsers().then(users => {
      const user = users.find(
        user => user.email === email && user.password === password
      )

      if (!user) {
        return Alert.alert(
          'Conta não encontrado',
          'Você deve criar uma conta para prosseguir'
        )
      }

      setUser(user)
      navigate('Home')
    })
  }

  const handleFormSubmit = () => formRef.current.submitForm()

  return (
    <Container>
      <FormWrapper>
        <Form ref={formRef} onSubmit={handleStoreNewUser}>
          <Input name="email" placeholder="email" autoComplete="email" />
          <InputDivisor />
          <Input
            name="password"
            placeholder="senha"
            autoComplete="password"
          />
          <InputDivisor />
        </Form>
      </FormWrapper>
      <ButtonWrapper>
        <Button activeOpacity={0.5} onPress={() => handleFormSubmit()}>
          <ButtonText>Entrar</ButtonText>
        </Button>
      </ButtonWrapper>
      <Button
        backgroundColor={colors.purple400}
        activeOpacity={0.5}
        onPress={() => navigate('SignIn')}
      >
        <ButtonText fontSize={16} color={colors.white}>
          Cadastrar conta
        </ButtonText>
      </Button>
    </Container>
  )
})
