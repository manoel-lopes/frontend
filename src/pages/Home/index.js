import React, { useEffect, useState } from 'react'
import { Alert } from 'react-native'

import { connect } from 'react-redux'

import { setNewUser } from '~/store/actions/user'
import { setNewTasks } from '~/store/actions/tasks'

import { isValidDate } from '~/util/isValidDate'
import { parseDate } from '~/util/parseDate'

import { api } from '~/services/api'

import { Header } from '~/components/Header'
import { TasksList } from '~/components/TasksList'
import { TodoInput } from '~/components/TodoInput'

import { Container } from './styles'

const mapStateToProps = state => {
  return {
    user: state.user,
    tasks: state.tasks
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: newUser => {
      const action = setNewUser(newUser)
      dispatch(action)
    },
    setTasks: newTasks => {
      const action = setNewTasks(newTasks)
      dispatch(action)
    }
  }
}

export const Home = connect(
  mapStateToProps,
  mapDispatchToProps
)(({ user, setUser, tasks, setTasks }) => {
  const [shouldFilterDoneTasks, setShouldFilterDoneTasks] = useState(false)
  const [doneTasks, setDoneTasks] = useState([])

  const [shouldFilterNotDoneTasks, setShouldFilterNotDoneTasks] =
    useState(false)
  const [notDoneTasks, setNotDoneTasks] = useState([])

  const handlerUserTasksUpdate = user => {
    setUser(user)
    api.updateUser(user)
  }

  const handleTaskAddition = ({ title, description, date, hour }) => {
    const hasTaskWithSameTitle = tasks.find(task => task.title === title)

    if (hasTaskWithSameTitle) {
      return Alert.alert(
        'Tarefa já cadastrada',
        'Você não pode cadastrar mais de uma tarefa com o mesmo título'
      )
    }

    if (date && !isValidDate(date)) {
      return Alert.alert('Data inválida', 'Digite uma data válida')
    }

    if (hour && hour.length !== 5) {
      return Alert.alert('Hora inválida', 'Digite uma hora válida')
    }

    if (!title || !description || !date || !hour) {
      return Alert.alert(
        'Campos obrigatórios',
        'Você deve preecncher todos os campos para cadastrar a tarefa'
      )
    }

    const newTask = {
      id: new Date().getTime(),
      done: false,
      title,
      description,
      date,
      hour
    }

    handlerUserTasksUpdate({ ...user, tasks: [...tasks, newTask] })
  }

  const handleTasksSortingByDate = () => {
    setShouldFilterDoneTasks(false)
    setShouldFilterNotDoneTasks(false)

    const cloneTask = task => ({ ...task })
    const sortTasks = tasks.map(cloneTask)

    sortTasks.sort((taskA, taskB) => {
      const dateA = parseDate(taskA.date, taskA.hour)
      const dateB = parseDate(taskB.date, taskB.hour)
      return dateA - dateB
    })

    handlerUserTasksUpdate({ ...user, tasks: sortTasks })
  }

  const handleDoneTasksFiltering = () => {
    setShouldFilterDoneTasks(true)
    setShouldFilterNotDoneTasks(false)
    setDoneTasks(tasks.filter(task => task.done))
  }

  const handleNotDoneTasksFiltering = () => {
    setShouldFilterNotDoneTasks(true)
    setShouldFilterDoneTasks(false)
    setNotDoneTasks(tasks.filter(task => !task.done))
  }

  const handleTaskDoneToggle = id => {
    const cloneTask = task => ({ ...task })
    const updatedTasks = tasks.map(cloneTask)

    const taskToBeUpdated = updatedTasks.find(task => task.id === id)

    if (!taskToBeUpdated) {
      return
    }

    taskToBeUpdated.done = !taskToBeUpdated.done
    handlerUserTasksUpdate({ ...user, tasks: updatedTasks })
  }

  const handleTaskEdition = (id, newTitle) => {
    const cloneTask = task => ({ ...task })
    const updatedTasks = tasks.map(cloneTask)

    const taskToBeUpdated = tasks.find(task => task.id === id)

    if (!taskToBeUpdated) {
      return
    }

    taskToBeUpdated.title = newTitle
    handlerUserTasksUpdate({ ...user, tasks: updatedTasks })
  }

  const handleTaskRemoval = id => {
    Alert.alert(
      'Remover item',
      'Tem certeza que você deseja remover esse item?',
      [
        {
          style: 'cancel',
          text: 'Não'
        },
        {
          style: 'destructive',
          text: 'Sim',
          onPress: () =>
            handlerUserTasksUpdate({
              ...user,
              tasks: tasks.filter(task => task.id !== id)
            })
        }
      ]
    )
  }

  useEffect(() => setTasks(user.tasks), [user])

  return (
    <Container>
      <Header tasksCounter={tasks.length} />

      <TodoInput
        addTask={handleTaskAddition}
        filterDoneTasks={handleDoneTasksFiltering}
        filterNotDoneTasks={handleNotDoneTasksFiltering}
        orderTasksByDate={handleTasksSortingByDate}
      />

      <TasksList
        tasks={
          shouldFilterDoneTasks
            ? doneTasks
            : shouldFilterNotDoneTasks
            ? notDoneTasks
            : tasks
        }
        toggleTaskDone={handleTaskDoneToggle}
        removeTask={handleTaskRemoval}
        editTask={handleTaskEdition}
        shouldDisableTaskButton={
          shouldFilterDoneTasks || shouldFilterNotDoneTasks
        }
      />
    </Container>
  )
})
