import styled from 'styled-components/native'

import { colors } from '~/styles/global'

export const Container = styled.View`
  background-color: ${colors.gray100};
`
