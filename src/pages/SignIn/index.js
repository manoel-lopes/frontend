import React, { useRef } from 'react'
import { Alert } from 'react-native'

import { useNavigation } from '@react-navigation/core'

import { connect } from 'react-redux'

import { Form } from '@unform/mobile'

import { api } from '~/services/api'

import { setNewUser } from '~/store/actions/user'

import { Input } from '~/components/Input'

import {
  Container,
  FormWrapper,
  InputDivisor,
  ButtonWrapper,
  Button,
  ButtonText
} from './styles'

const mapDispatchToProps = dispatch => {
  return {
    setUser: newUser => {
      const action = setNewUser(newUser)
      dispatch(action)
    }
  }
}

export const SignIn = connect(
  null,
  mapDispatchToProps
)(({ setUser }) => {
  const formRef = useRef(null)
  const { navigate } = useNavigation()

  const handleStoreNewUser = user => {
    const { name, email, password, birthDate, cpf } = user

    if (!name || !email || !password || !birthDate || !cpf) {
      return Alert.alert(
        'Campos obrigatórios',
        'Você deve preecncher todos os campos para cadastrar a tarefa'
      )
    }

    const emailTest = /[\w.]+@[\w]+\.[a-z]+(\.[a-z]+)?$/i
    const isValidEmail = emailTest.test(email)

    if (email && !isValidEmail) {
      return Alert.alert(
        'Email inválido',
        'Você deve digitar um email válido'
      )
    }

    if (cpf.length !== 11) {
      return Alert.alert('CPF inválido', 'Digite um CPF válido')
    }

    const newUser = { id: new Date().getTime(), ...user, tasks: [] }

    api.getUsers().then(users => {
      const isCPFAlreadyInUse = users.find(user => user.cpf === cpf)

      if (isCPFAlreadyInUse) {
        return Alert.alert(
          'CPF já cadastrado',
          'Você já possui uma conta cadastrada'
        )
      }

      const isEmailAlreadyInUse = users.find(user => user.email === email)

      if (isEmailAlreadyInUse) {
        return Alert.alert(
          'Email já cadastrado',
          'Você já possui uma conta com esse email'
        )
      }

      api.storeUser(newUser).then(() => {
        navigate('Home')
        setUser(newUser)
      })
    })
  }

  const handleFormSubmit = () => formRef.current.submitForm()

  return (
    <Container>
      <FormWrapper>
        <Form ref={formRef} onSubmit={handleStoreNewUser}>
          <Input name="name" placeholder="nome" autoComplete="name" />
          <InputDivisor />
          <Input name="email" placeholder="email" autoComplete="email" />
          <InputDivisor />
          <Input
            name="password"
            placeholder="senha"
            autoComplete="password"
          />
          <InputDivisor />
          <Input
            name="birthDate"
            placeholder="data de nascimento"
            autoComplete="birthdate-full"
            maxLength={10}
          />
          <InputDivisor />
          <Input name="cpf" placeholder="cpf" maxLength={11} />
          <InputDivisor />
        </Form>
      </FormWrapper>
      <ButtonWrapper>
        <Button activeOpacity={0.7} onPress={() => handleFormSubmit()}>
          <ButtonText>Cadastrar</ButtonText>
        </Button>
      </ButtonWrapper>
    </Container>
  )
})
