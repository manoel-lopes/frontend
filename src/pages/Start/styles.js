import styled from 'styled-components/native'

import { colors } from '~/styles/global'

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: ${colors.purple400};
`

export const ButtonWrapper = styled.View`
  top: -30px;
  align-items: center;
  justify-content: center;
`

export const Button = styled.TouchableOpacity`
  height: 52px;
  width: 165px;
  background-color: ${colors.white};
  justify-content: center;
  align-items: center;
  margin-top: 15px;
  border-radius: 25px;
  border-top-right-radius: 25px;
  border-bottom-right-radius: 25px;
`

export const ButtonText = styled.Text`
  font-size: 22px;
  font-weight: bold;
`
export const DescriptionWrapper = styled.View`
  justify-content: center;
  align-items: center;
  margin-bottom: 40px;
`
export const Description = styled.Text`
  font-size: 28px;
  color: ${colors.white};
`
