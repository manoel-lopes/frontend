import React from 'react'

import { useNavigation } from '@react-navigation/core'

import {
  Container,
  ButtonWrapper,
  Button,
  ButtonText,
  DescriptionWrapper,
  Description
} from './styles'

export const Start = () => {
  const { navigate } = useNavigation()

  return (
    <Container>
      <DescriptionWrapper>
        <Description>Organizar as suas tarefas</Description>
        <Description>nunca foi tão simples e fácil!</Description>
      </DescriptionWrapper>
      <ButtonWrapper>
        <Button activeOpacity={0.7} onPress={() => navigate('Login')}>
          <ButtonText>Entrar</ButtonText>
        </Button>
        <Button activeOpacity={0.7} onPress={() => navigate('SignIn')}>
          <ButtonText>Criar Conta</ButtonText>
        </Button>
      </ButtonWrapper>
    </Container>
  )
}
