export const fonts = {
  regular: 'Inter_400Regular',
  medium: 'Inter_500Medium',
  bold: 'Inter_700Bold'
}

export const colors = {
  purple400: '#8257E5',
  green400: '#1DB863',
  gray500: '#666',
  gray200: '#b2b2b2',
  gray100: '#ebebeb',
  white: '#fff',
  black: '#000'
}
