const initialState = {
  name: '',
  email: '',
  password: '',
  birthDate: '',
  cpf: '',
  tasks: []
}

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'A_NEW_USER_WAS_SET':
      return {
        ...state,
        ...action.payload
      }

    default:
      return state
  }
}
