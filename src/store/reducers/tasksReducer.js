const initialState = []

export const tasksReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'TASKS_CHANGED':
      return [...action.payload]

    default:
      return state
  }
}
