import { combineReducers, createStore } from 'redux'

import { userReducer } from './reducers/userReducer'
import { tasksReducer } from './reducers/tasksReducer'

const reducers = combineReducers({
  user: userReducer,
  tasks: tasksReducer
})

export const store = createStore(reducers)
