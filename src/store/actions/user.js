export const setNewUser = newUser => {
  return {
    type: 'A_NEW_USER_WAS_SET',
    payload: newUser
  }
}
