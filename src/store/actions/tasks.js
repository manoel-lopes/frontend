export const setNewTasks = newTasks => {
  return {
    type: 'TASKS_CHANGED',
    payload: newTasks
  }
}
