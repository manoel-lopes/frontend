import { parseDate } from './parseDate'

export const isValidDate = datestr => {
  const date = parseDate(datestr)
  return (
    datestr.length === 10 &&
    date &&
    date.getMonth() + 1 == Number(datestr.split('/')[1])
  )
}
