export const parseDate = (datestr, hourstr = '') => {
  const [day, month, year] = datestr.split('/')
  const [hours, minutes] = hourstr.split(':')

  if (hourstr) {
    return new Date(year, month - 1, day, hours, minutes)
  }
  return new Date(year, month - 1, day)
}
