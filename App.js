import React from 'react'
import { StatusBar } from 'react-native'

import { Provider } from 'react-redux'

import AppLoading from 'expo-app-loading'

import {
  useFonts,
  Inter_400Regular,
  Inter_500Medium,
  Inter_700Bold
} from '@expo-google-fonts/inter'

import { Routes } from '~/routes'

import { store } from '~/store'

export default function App() {
  const [fontsLoaded] = useFonts({
    Inter_400Regular,
    Inter_500Medium,
    Inter_700Bold
  })

  if (!fontsLoaded) {
    return <AppLoading />
  }

  return (
    <>
      <StatusBar
        barStyle="light-content"
        translucent
        backgroundColor="transparent"
      />
      <Provider store={store}>
        <Routes />
      </Provider>
    </>
  )
}
